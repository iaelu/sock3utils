package sock3utils

import (
	"bytes"
	"encoding/binary"
	"net"
)

func Envoi(conn net.Conn, b []byte, sz int) error {
	for pos := 0; pos < sz; {
		n, err := conn.Write(b[pos:sz])
		if err != nil {
			return err
		}
		pos += n
	}
	return nil
}

// Puts helps to write a buffer to a program using libsock3
func Puts(conn net.Conn, b []byte) error {
	buffer := bytes.NewBuffer([]byte{})
	binary.Write(buffer, binary.LittleEndian, int32(len(b)))
	buf := buffer.Bytes()
	if err := Envoi(conn, buf, len(buf)); err != nil {
		return err
	}
	if err := Envoi(conn, b, len(b)); err != nil {
		return err
	}
	return nil
}

func Recoit(conn net.Conn, b []byte, sz int) error {
	pos := 0
	for pos < sz {
		i, err := conn.Read(b[pos:sz])
		if err != nil {
			return err
		}
		pos += i
	}
	return nil
}

func Agets(conn net.Conn) ([]byte, error) {
	i32sz := binary.Size(int32(0))
	dlen := make([]byte, i32sz)
	if err := Recoit(conn, dlen, i32sz); err != nil {
		return nil, err
	}
	buf := bytes.NewReader(dlen)
	var length int32
	if err := binary.Read(buf, binary.LittleEndian, &length); err != nil {
		return nil, err
	}
	if length < 0 {
		return nil, nil
	}
	data := make([]byte, length)
	if err := Recoit(conn, data, int(length)); err != nil {
		return nil, err
	}
	return data, nil
}

// Gets helps to get a buffer sent from a program using libsock3
func Gets(conn net.Conn, b []byte) (int, error) {
	i32sz := binary.Size(int32(0))
	if err := Recoit(conn, b, i32sz); err != nil {
		return -1, err
	}
	buf := bytes.NewReader(b)
	var length int32
	if err := binary.Read(buf, binary.LittleEndian, &length); err != nil {
		return -1, err
	}
	n := int(length)
	if n > len(b) {
		n = len(b)
	}
	if err := Recoit(conn, b, n); err != nil {
		return -1, err
	}
	return n, nil
}
