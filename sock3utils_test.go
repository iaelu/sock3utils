package sock3utils

import (
	"bytes"
	"fmt"
	"net"
	"testing"
)

func TestItWorks(t *testing.T) {
	addr := "127.0.0.1:7658"
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		t.Fatal(err)
	}
	defer ln.Close()

	msg := make(chan string)
	goerr := make(chan error, 1)

	go func() {
		cn, err := net.Dial("tcp", addr)
		if err != nil {
			goerr <- err
			return
		}
		defer cn.Close()

		buf := make([]byte, 17)
		n, err := Gets(cn, buf)
		if err != nil {
			goerr <- err
			return
		}
		msg <- fmt.Sprintf("[%s]#%d", buf, n)

		if err := Puts(cn, []byte("toto et tata font du ski")); err != nil {
			goerr <- err
			return
		}
	}()

	cn, err := ln.Accept()
	if err != nil {
		t.Fatal(err)
	}
	defer cn.Close()

	if err := Puts(cn, []byte("titi fait du velo")); err != nil {
		t.Fatal(err)
	}
	select {
	case str := <-msg:
		if str != "[titi fait du velo]#17" {
			t.Fatalf("expected %q, got %q", "[titi fait du velo]#17", str)
		}
	case e := <-goerr:
		t.Fatalf("gorouting err: %v", e)
	}

	buf := make([]byte, 24)
	n, err := Gets(cn, buf)
	if err != nil {
		t.Fatal(err)
	}
	if n != 24 {
		t.Fatalf("expected 24, got %d", n)
	}
	if !bytes.Equal(buf, []byte("toto et tata font du ski")) {
		t.Fatalf(`expected %q, got %q`, []byte("toto et tata font du ski"), buf)
	}
}
