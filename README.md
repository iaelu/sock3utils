# sock3utils

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/iaelu/sock3utils)](https://goreportcard.com/report/gitlab.com/iaelu/sock3utils)

# Description
Simple package that mimics C functions from a library called libsock3.

See [samples](samples/)
